#source
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
source ~/.zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

#aliases
alias ll="ls -lha"
alias greo="grep --color=auto"
alias zrc="vim ~/.zshrc"
alias c="clear"
alias ls="ls --color=auto"
alias gic="git clone"
alias pac="sudo pacman "
alias ..="cd .."

#tab selection
autoload -Uz compinit
compinit
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' menu select
zstyle ':completion:*:select:*' menu yes select-color '%F{blue}'
zstyle ':completion:*:select:*' menu no select-color '%F{default}'

#bindkey
bindkey '\e[1~' beginning-of-line
bindkey '\e[4~' end-of-line
bindkey '^[[A' history-beginning-search-backward
bindkey '^[[B' history-beginning-search-forward

#for smth like oh-my-zsh
setopt no_case_glob
setopt auto_cd

#prompt
PROMPT='%F{cyan}%n@%m%~%f:~$'

